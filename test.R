library(sp)
library(raster)
library(testit)

a <- rbind(c(-1, -1), c(1, 1))
a_low <- a - 100
a_high <- a + 100

assert(
    "a DEM's surface is the same regardless its relative height",
    surfaceArea(a) == surfaceArea((a_low)) &&
    surfaceArea(a) == surfaceArea((a_high))
)