# Calculating surface areas based on digital elevation models
Testing sp's `surfaceArea` function for calculating the actual surface area of given DEMs (digital elevation models).

## Requirements

### Data
Requires DEMs as `.tiff` files (*geotiffs*) in a ```input``` directory.

### R packages

- `raster`
- `sp`
- `testit`